var express = require('express');
var app = express();
var mongojs = require('mongojs');
var db = mongojs('UKIemployees', ['employees']);
var bodyParser = require('body-parser');

app.use(express.static(__dirname + '/public'));
app.use(bodyParser.json());

app.get('/employees', function (req, res) {
  console.log('I received a GET request');

  db.employees.find(function (err, docs) {
    console.log(docs);
    res.json(docs);
  });
});

app.post('/employees', function (req, res) {
  console.log(req.body);
  db.employees.insert(req.body, function(err, doc) {
    res.json(doc);
  });
});

app.delete('/employees/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.employees.remove({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.get('/employees/:id', function (req, res) {
  var id = req.params.id;
  console.log(id);
  db.employees.findOne({_id: mongojs.ObjectId(id)}, function (err, doc) {
    res.json(doc);
  });
});

app.put('/employees/:id', function (req, res) {
  var id = req.params.id;
  // console.log(req.body.name);
  db.employees.findAndModify({
    query: {_id: mongojs.ObjectId(id)},
    update: {$set: {code_employee: req.body.code_employee, employeename: req.body.employeename, department: req.body.department}},
    new: true}, function (err, doc) {
      res.json(doc);
    }
  );
});

app.listen(4200);
console.log("Server running on port 3000");